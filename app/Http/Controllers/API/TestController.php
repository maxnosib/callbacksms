<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function test3 ($id)
    {
        return [ 'data' => $id, 'result' => true ];
    }
    public function test4 (Request $request)
    {
        $req = $request->input('link');
        return [ 'data' => $req, 'result' => true ];
    }
}
