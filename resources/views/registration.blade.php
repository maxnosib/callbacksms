<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <link rel="stylesheet" href="/css/vendor.css">
        <link rel="stylesheet" href="/css/app.css">
    </head>
    <body ng-app="BusiaApp" >
        <div ng-view></div>

        <script src="/js/vendor.js"></script>
        <script src="/js/app.js"></script>

    </body>
</html>
