angular
    .module('BusiaApp', [
        'ngRoute', 'ngDialog'
    ])
    .config([
        '$routeProvider', '$locationProvider',
        function($routeProvider, $locationProvider) {
            $routeProvider
                //.when('/test', { controller: 'TestCtrl', templateUrl: 'html/test.html' })
                //.when('/registration', { controller: 'RegCtrl', templateUrl: 'html/registration.html' })
                .otherwise({ redirectTo: '/' });

            $locationProvider.html5Mode({ enabled: true, requireBase: false });
        }
    ]);
    
    /*
     * 
     * тут прописывать контроллеры и въюхи для роутов
     * 
     * 
     */