angular.module('BusiaApp')     
    .controller('TestCtrl', ['$rootScope', '$scope', '$http', '$location', '$sce',
        function ($rootScope, $scope, $http, $location, $sce) {
            console.log('test controller');
            $scope.test = function ()
            {
                console.log('open test function');
            };

            $scope.test2 = function (id)
            {
                $http.get('/test2/test3'+id)
                    .then(function (res) {
                        console.log('open test3 then',res.data);
                    })
                    .catch(function (res) {
                        console.log('open test3 catch',res);
                    })
                    .finally(function (res) {
                        console.log('open test3 finally',res);
                    });
            };
            $scope.test3 = function ()
            {
                $http.post('/test2/test4', { link: '$scope.youTubeLink' })
                    .then(function (res) {
                        console.log('open test4 then',res.data);
                    })
                    .catch(function (res) {
                        console.log('open test4 catch',res);
                    })
                    .finally(function (res) {
                        console.log('open test4 finally',res);
                    });
            };
        }
    ]);

/*
 * 
 * это контроллер для каждой въюхт должен быть свой
 * 
 */

/*
 * 

 * 
 */