<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function () {
    return view('welcome');
});
/*
Route::get('/registration', function () {
    return view('auth.register');
});
*/
//Route::post('/authorization', [ 'uses' => 'LoginController@login' ]);

Route::group([ 'prefix' => '/test2' ], function() {

    Route::get('/test3{id}',  [ 'uses' => 'API\TestController@test3' ]);
    Route::post('/test4', [ 'uses' => 'API\TestController@test4' ]);

});






