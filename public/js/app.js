angular
    .module('BusiaApp', [
        'ngRoute', 'ngDialog'
    ])
    .config([
        '$routeProvider', '$locationProvider',
        function($routeProvider, $locationProvider) {
            $routeProvider
                //.when('/test', { controller: 'TestCtrl', templateUrl: 'html/test.html' })
                //.when('/registration', { controller: 'RegCtrl', templateUrl: 'html/registration.html' })
                .otherwise({ redirectTo: '/' });

            $locationProvider.html5Mode({ enabled: true, requireBase: false });
        }
    ]);
    
    /*
     * 
     * тут прописывать контроллеры и въюхи для роутов
     * 
     * 
     */
/*
window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */
/*
try {
    window.$ = window.jQuery = require('jquery');

    require('bootstrap-sass');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */
/*
window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */
/*
let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: 'your-pusher-key'
// });

angular
    .module('BusiaApp')
    .controller('AppCtrl', [
        '$rootScope', '$scope', '$http', '$sce', '$timeout', '$location', '$routeParams', 'ngDialog',
        function($rootScope, $scope, $http, $sce, $timeout, $location, $routeParams, ngDialog) {

            /*
            $rootScope.share = false;
            $rootScope.showSimulation = '';
            $rootScope.showVideoPlayer = '';
            $rootScope.showSettings = false;
            $rootScope.showYouTubeModal = false;

            $scope.showRename = false;
            $scope.canvasShow = false;
            $scope.myVideosType = false;
            $scope.videoNotFound = false;
            $scope.showSendEmail = false;
            $scope.canSharePlaylist = false;
            $scope.shawAllNotification = false;


            $scope.sendEmailModalClose = function () {
                $scope.showSendEmail = false;
                $scope.sendEmailSuccess = -1;
            };


            $scope.loadPlaylists = function () {
                $http.get('/api/get_playlists').success(function (res) {
                    if (res.result) {

                    } else {
                        $scope.playlists = { };
                    }
                });
            };
            
            
            $(document).mouseup(function (e) {
                $scope.hidePopUpMenu(e);
            });

            document.addEventListener('touchstart', function(e) {
                $scope.hidePopUpMenu(e);
            }, false);

            $(document).ready(function () {
                $('html').rightClick( function(e) {
                    return false;
                });
            });

            window.onkeydown = function (evt) {
                if(evt.keyCode == 123) return false;
            };

            window.onkeypress = function (evt) {
                if(evt.keyCode == 123) return false;
            };

            $(window).resize(function (ev) {
                if (ev.currentTarget.innerWidth < 1440) {
                    $scope.carouselMaxNum = 2;
                    $scope.lastCarouselItem = 1;
                }
            });

            if (screen.width < 1440) {
                $scope.carouselMaxNum = 2;
                $scope.lastCarouselItem = 1;
            }

            if ($scope.user) {
                angular.element(document.querySelector(".b-box__head")).css('background-color', $scope.user.color);
            }
*/

        }

    ]);
angular.module('BusiaApp')     
    .controller('RegCtrl', ['$rootScope', '$scope', '$http', '$location', '$sce', 'ngDialog',
        function ($rootScope, $scope, $http, $location, $sce, ngDialog) {
            console.log('test controller');
            $scope.logForm = {
                login: '',
                password: ''
            };

            $scope.login = function () {
                console.log($scope.logForm);
                
                var login    = $scope.logForm.login.trim(),
                    password = $scope.logForm.password.trim();

                if (!login && !password) {
                    $scope.message = 'You must enter login and password';

                    ngDialog.open({
                        template: '/html/modal/message.html',
                        className: 'ngdialog-theme-default',
                        scope: $scope
                    });

                    return false;
                }
/*
                $http.post('/authorization', { email: login, password: password }).success(function (res) {
                    if (res.result) {
                        $http.get('/api/license').success(function (res) {
                            if (res.result) {
                                window.location = '/?licenseKey=' + res.data.key;
                            }
                        });
                    } else {
                        $scope.message = res.message;

                        ngDialog.open({
                            template: '/html/modal/message.html',
                            className: 'ngdialog-theme-default',
                            scope: $scope
                        });
                    }
                });*/
            };
        }

    ]);

/*
 * 
 * это контроллер для каждой въюхт должен быть свой
 * 
 */

/*
 * 

 * 
 */
angular.module('BusiaApp')     
    .controller('TestCtrl', ['$rootScope', '$scope', '$http', '$location', '$sce',
        function ($rootScope, $scope, $http, $location, $sce) {
            console.log('test controller');
            $scope.test = function ()
            {
                console.log('open test function');
            };

            $scope.test2 = function (id)
            {
                $http.get('/test2/test3'+id)
                    .then(function (res) {
                        console.log('open test3 then',res.data);
                    })
                    .catch(function (res) {
                        console.log('open test3 catch',res);
                    })
                    .finally(function (res) {
                        console.log('open test3 finally',res);
                    });
            };
            $scope.test3 = function ()
            {
                $http.post('/test2/test4', { link: '$scope.youTubeLink' })
                    .then(function (res) {
                        console.log('open test4 then',res.data);
                    })
                    .catch(function (res) {
                        console.log('open test4 catch',res);
                    })
                    .finally(function (res) {
                        console.log('open test4 finally',res);
                    });
            };
        }
    ]);

/*
 * 
 * это контроллер для каждой въюхт должен быть свой
 * 
 */

/*
 * 

 * 
 */
//# sourceMappingURL=app.js.map
