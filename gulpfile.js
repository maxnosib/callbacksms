const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

require('es6-promise').polyfill();

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix
        /**
         * App Styles
         */
        //.sass('client/app.scss', 'public/css/app.css')
        .sass('/app.scss', 'public/css/app.css')
        /**
         * Admin Styles
         */
    	//.sass('admin/app.scss', 'public/css/admin.css')

        /**
         * App Vendor Styles
         */
        .styles([
            '../../../bower_components/ng-dialog/css/ngDialog.min.css',
            '../../../bower_components/ng-dialog/css/ngDialog-theme-default.min.css',
        ], 'public/css/vendor.css')
        
        /**
         * App Scripts
         */
        .scripts([
            '/**/*.js'
        ], 'public/js/app.js')
        /**
         * Admin Scripts
         */
        //.scripts([
        //    'admin/**/*.js'
        //], 'public/js/admin.js')

        /**
         * App Vendor Scripts
         */
        .scripts([
            '../../../bower_components/jquery/dist/jquery.min.js',
            '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
            '../../../bower_components/angular/angular.min.js',
            '../../../bower_components/angular-route/angular-route.min.js',
            '../../../bower_components/ng-dialog/js/ngDialog.min.js',
            '../../../bower_components/angular-bootstrap/ui-bootstrap.min.js',
        ], 'public/js/vendor.js')
        /**
         * Admin Vendor Scripts
         */
        .scripts([
            '../../../bower_components/jquery/dist/jquery.min.js',
            '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
            '../../../bower_components/angular/angular.min.js',
            '../../../bower_components/angular-route/angular-route.min.js',
            '../../../bower_components/ng-dialog/js/ngDialog.min.js'
        ], 'public/js/admin.vendor.js');
});
